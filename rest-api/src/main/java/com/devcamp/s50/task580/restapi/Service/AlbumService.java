package com.devcamp.s50.task580.restapi.Service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.devcamp.s50.task580.restapi.model.Album;
@Service
public class AlbumService {

    public List<String> getListSongs() {
        List<String> songs = new ArrayList<>();
        songs.add("hello");
        songs.add("My world");
        songs.add("Big girl don't cry");
        return songs;
    }
    
    Album album1 = new Album("Vol.1", this.getListSongs());
    Album album2 = new Album("Vol.2", this.getListSongs());
    Album album3 = new Album("Vol.3", this.getListSongs());
    Album album4 = new Album("Vol.4", this.getListSongs());
    Album album5 = new Album("Vol.5", this.getListSongs());
    Album album6 = new Album("Vol.6", this.getListSongs());
    Album album7 = new Album("Vol.7", this.getListSongs());
    Album album8 = new Album("Vol.8", this.getListSongs());
    Album album9 = new Album("Vol.9", this.getListSongs());
    Album album10 = new Album("Vol.10", this.getListSongs());
    
    public List<Album> getAlbumList() {
        List<Album> albumList = new ArrayList<>();
        albumList.add(album1);
        albumList.add(album2);
        albumList.add(album3);
        albumList.add(album4);
        albumList.add(album5);
        albumList.add(album6);
        albumList.add(album7);
        albumList.add(album8);
        albumList.add(album9);
        albumList.add(album10);
        return albumList;
    }

}
