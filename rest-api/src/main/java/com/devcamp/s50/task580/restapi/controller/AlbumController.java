package com.devcamp.s50.task580.restapi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s50.task580.restapi.Service.AlbumService;
import com.devcamp.s50.task580.restapi.model.Album;

@RestController
@CrossOrigin
@RequestMapping("/")
public class AlbumController {
    @Autowired
    AlbumService albumService;
    
    @GetMapping("/albums")
    public List<Album> getAllAlbums() {
       return albumService.getAlbumList();
    }
}
