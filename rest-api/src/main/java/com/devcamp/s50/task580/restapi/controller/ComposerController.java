package com.devcamp.s50.task580.restapi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s50.task580.restapi.Service.ComposerService;
import com.devcamp.s50.task580.restapi.model.Band;
import com.devcamp.s50.task580.restapi.model.Composer;


@RestController
@CrossOrigin
@RequestMapping("/")
public class ComposerController {
    @Autowired
    ComposerService composerService;
    
    @GetMapping("/bands")
    public List<Band> getBandList() {
        return composerService.getBandList();
    }

    @GetMapping("/artists")
    public List<Composer> getArtistList() {
        return composerService.getComposerList();
    }
}
