package com.devcamp.s50.task580.restapi.model;

import java.util.List;

public class Album {
    private String name;
    List<String> songs;
    public Album() {
    }
    public Album(String name, List<String> songs) {
        this.name = name;
        this.songs = songs;
    }
    public String getAlbumName() {
        return name;
    }
    public void setAlbumName(String name) {
        this.name = name;
    }
    public List<String> getSongs() {
        return songs;
    }
    public void setSongs(List<String> songs) {
        this.songs = songs;
    }
   
    

}
