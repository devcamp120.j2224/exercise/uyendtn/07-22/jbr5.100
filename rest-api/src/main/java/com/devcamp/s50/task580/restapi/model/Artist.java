package com.devcamp.s50.task580.restapi.model;

import java.util.List;

public class Artist extends Composer {
    private List<Album> albums;

    public Artist() {
    }

    public Artist(List<Album> albums) {
        this.albums = albums;
    }

    public Artist(String stagename, List<Album> albums) {
        super(stagename);
        this.albums = albums;
    }

    public Artist(String firstname, String lastname, String stagename, List<Album> albums) {
        super(firstname, lastname, stagename);
        this.albums = albums;
    }

    public List<Album> getAlbums() {
        return albums;
    }

    public void setAlbums(List<Album> albums) {
        this.albums = albums;
    }
    
}
