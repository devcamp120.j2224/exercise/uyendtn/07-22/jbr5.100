package com.devcamp.s50.task580.restapi.model;

import java.util.List;

public class Band  {
    private String bandName;
    private List<Bandmember> members;
    private List<Album> albums;
    public Band() {
    }
    public Band(String bandName, List<Bandmember> members, List<Album> albums) {
        this.bandName = bandName;
        this.members = members;
        this.albums = albums;
    }
    public String getBandName() {
        return bandName;
    }
    public void setBandName(String bandName) {
        this.bandName = bandName;
    }
    public List<Bandmember> getMembers() {
        return members;
    }
    public void setMembers(List<Bandmember> members) {
        this.members = members;
    }
    public List<Album> getAlbums() {
        return albums;
    }
    public void setAlbums(List<Album> albums) {
        this.albums = albums;
    }

    
}
