package com.devcamp.s50.task580.restapi.model;

public class Composer extends Person {
    private String stagename;

    public Composer() {
    }

    public Composer(String stagename) {
        this.stagename = stagename;
    }

    public Composer(String firstname, String lastname, String stagename) {
        super(firstname, lastname);
        this.stagename = stagename;
    }

    public String getStagename() {
        return stagename;
    }

    public void setStagename(String stagename) {
        this.stagename = stagename;
    }

    
}
